package at.mavila.beanloading;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ClassToBeLoadedSpecialRenamed implements ApplicationClassLoader {

  @Override
  public String getMessage() {
    return "Hello World Special";
  }
}
