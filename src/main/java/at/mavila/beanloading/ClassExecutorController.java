package at.mavila.beanloading;

import org.springframework.context.ApplicationContext;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ClassExecutorController {

  private final ApplicationContext applicationContext;
  private final ApplicationConfiguration applicationConfiguration;

  public ClassExecutorController(final ApplicationContext applicationContext,
                                 final ApplicationConfiguration applicationConfiguration) {
    this.applicationContext = applicationContext;
    this.applicationConfiguration = applicationConfiguration;
  }

  @GetMapping("/printMessage")
  String all() {
    Class<ApplicationClassLoader> loadClass = this.applicationConfiguration.getLoadClass();
    ApplicationClassLoader bean = this.applicationContext.getBean(loadClass);
    return bean.getMessage();
  }

}
