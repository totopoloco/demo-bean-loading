package at.mavila.beanloading;


@FunctionalInterface
public interface ApplicationClassLoader {

  /**
   * Gets the message.
   *
   * @return a message.
   */
  String getMessage();

}
