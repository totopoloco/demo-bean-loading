package at.mavila.beanloading;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeanloadingApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeanloadingApplication.class, args);
	}

}
